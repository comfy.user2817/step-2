import gulp from 'gulp'
//===================Folders, Watchers & Deletion==================
import del from 'gulp-clean';
import onChange from 'gulp-changed';
import { compareContents } from 'gulp-changed';
import plumber from "gulp-plumber";
import notify from "gulp-notify";
import browserSync from 'browser-sync';
import fs from 'fs'

//===================HTML===================
import htmlImport from 'gulp-html-import';
import bemValid from 'gulp-html-bem-validator';
import htmlMin from 'gulp-htmlmin';

//===================SCSS===================
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import autoPrefixer from 'gulp-autoprefixer';
import gCmQ from 'gulp-group-css-media-queries';
import purgeCss from 'gulp-purgecss';
import miniCss from 'gulp-csso';

//===================JS======================
import terser from "gulp-terser";
import concat from "gulp-concat";

//===================IMAGES==================
// import imgMin from 'gulp-imagemin';



const { src, dest, watch, series, parallel } = gulp;
const sass = gulpSass(dartSass);




// // ==============================Different Options==============================
// const plumberOptions = (title) => {
//     return {
//         errorHandler: notify.onError({
//             title: title,
//             message: "Error: <%= error.message %>",
//             sound: false
//         })

//     }
// }


const plumberOptions = (taskName) => ({
    errorHandler: notify.onError({
        title: `Error in ${taskName}`,
        message: '<%= error.message %>',
        sound: 'Exclamation'
    }),
    emitError: false // Продолжить выполнение задачи при ошибках
});








const purgeOptions = {

    content: ['src/**/*.html']

};



// ==========================================TASKS===========================================
const delTask = (done) => {
    if (fs.existsSync("./dist")) {
        return src("./dist", { read: false, allowEmpty: true })
            .pipe(del({ force: true }));
    }
    done();

};





const htmlTask = () => {
    return src('./src/html/index.html')
        .pipe(onChange('./dist', { hasChanged: compareContents }))
        .pipe(plumber(plumberOptions("HTML")))
        .pipe(htmlImport('./src/html/components/'))
        .pipe(bemValid())
        .pipe(htmlMin({ collapseWhitespace: true }))
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true }))
        .on('end', () => {
            console.log('\x1b[35m%s\x1b[0m', 'Страница перезагружена успешно.');
        });

};





const scssTask = () => {
    return src('./src/scss/style.scss')
        .pipe(onChange('./dist/css', { hasChanged: compareContents }))
        .pipe(plumber(plumberOptions("SCSS")))
        .pipe(sass())
        .pipe(gCmQ())
        .pipe(autoPrefixer())
        .pipe(purgeCss(purgeOptions))
        .pipe(miniCss())
        .pipe(dest('./dist/css'))
        .pipe(browserSync.reload({ stream: true }))
        .on('end', () => {
            console.log('\x1b[35m%s\x1b[0m', 'Страница перезагружена успешно.');
        });
};



const jsTask = () => {
    return src('./src/js/**/*.js')
        .pipe(onChange('./dist/js', { hasChanged: compareContents }))
        .pipe(plumber(plumberOptions("JS")))
        .pipe(concat("index.js"))
        .pipe(terser())
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true }))
        .on('end', () => {
            console.log('\x1b[35m%s\x1b[0m', 'Страница перезагружена успешно.');
        });
};


const imgTask = () => {
    return src('./src/img/**/*')
        .pipe(onChange('./dist/img', { hasChanged: compareContents }))
        // .pipe(imgMin({ verbose: true, quality: 90, read: false }))
        .pipe(dest('./dist/img'))
        .pipe(browserSync.reload({ stream: true }))
        .on('end', () => {
            console.log('\x1b[35m%s\x1b[0m', 'Страница перезагружена успешно.');
        });
};


const fontsTask = () => {
    return src('./src/fonts/**/*').pipe(dest('./dist/fonts'));
}



const watchingTask = () => {
    browserSync.init({
        server: "./dist"
    });


    watch('./src/html/**/*.html', htmlTask);//.on('change', browserSync.reload);
    watch('./src/scss/**/*.scss', scssTask);
    watch('./src/js/**/*.js', jsTask);
    watch('./src/img/**/*', imgTask);
};

// =============================EXPORTS=============================


export const cleaning = delTask;
export const html = htmlTask;
export const css = scssTask;
export const js = jsTask;
export const img = imgTask;
export const font = fontsTask;
export const watcher = watchingTask;




export const dev = series(cleaning, parallel(html, css, js, font, img), watcher);

// export const dev = series(cleaning, parallel(html, css, js, parallel(font, img)), watcher);


